const express = require("express");
const router = express.Router();

const authController = require('../../controllers/api/authControllerAPI')

router.post('/autenticate', authController.autenticate);
router.post('/forgotPassword', authController.forgotPassword);

module.exports = router;