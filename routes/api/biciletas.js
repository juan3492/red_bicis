var express = require("express");
var router = express.Router();
var biciletasController = require("../../controllers/api/bicicletaControllerAPI");

router.get('/', biciletasController.bicicleta_list);
router.post('/create', biciletasController.bicicleta_create);
router.delete('/delete', biciletasController.bicicleta_delete);
router.put('/update', biciletasController.bicicleta_update);

module.exports = router;