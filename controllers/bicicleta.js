var Bicicleta = require('../models/bicicleta');


exports.bicicleta_list = function (req, res) {
    Bicicleta.allBicis(function (err, bicis) {
        if (err) console.log(err);
        res.render('bicicletas/index', {bicis: bicis});
    })
}

exports.bicicleta_create_get = function (req, res) {
    res.render('bicicletas/create');
}

exports.bicicleta_create_post = function (req, res) {
    var ubicacion = [req.body.lat, req.body.lng];
    var bici = {
        code: req.body.id, 
        color: req.body.color, 
        modelo: req.body.modelo,
        ubicacion: ubicacion
    };
    Bicicleta.add(bici, function (err) {
        if(err) console.log(err);
        res.redirect('/bicicletas')
    });
    
}

exports.bicicleta_update_get = function (req, res) {
    Bicicleta.findOne({_id: req.params.code}, function (err,bici) {
        res.render('bicicletas/update', {bici});
    });
}

exports.bicicleta_update_post = function (req, res) {
    Bicicleta.findByCode(req.params.code, function (err, bici) {
        console.log(bici);
        bici.color = req.body.color;
        bici.modelo = req.body.modelo;
        bici.ubicacion = [req.body.lat, req.body.lng];
        bici.save(function (err) {
            if (err) console.log(err);
            res.redirect('/bicicletas');
        });
    });
}

exports.bicicleta_delete_post = function(req, res) {
    Bicicleta.removeByCode(req.params.code, function (err) {
        if (err) console.log('error al eliminar bicicleta, err: ' + err);
        res.redirect('/bicicletas');
    });

} 