var mongoose    = require ('mongoose')
var Bicicleta = require('../../models/bicicleta');
var request= require('request');
var server = require('../../bin/www');

var base_url= "http://localhost:3000/api/bicicletas"

describe("Bicileta API", () => {
    beforeEach(function(done){
        var mongoDB = 'mongoDB://localhost/testdb';
        mongoose.connect(mongoDB, {useNewUrlParser: true});

        const db = mongoose.connection;
        db.on("error", console.error.bind(console, 'conection error'));
        db.once('open', function() {
            console.log('We are conected to test database');
            
            done();
        });
    });

    afterEach(function(done) {
        Bicicleta.deleteMany({}, function(err,success){
            if (err) console.log(err);
            mongoose.disconnect();
            done();
        })
    });

    beforeAll((done) => { mongoose.connection.close(done) });

    describe('GET BICICLETAS /', () => {
        it("Status 200", (done)=> {
            request.get(base_url, function(error,response,body){
                var result =JSON.parse(body);
                expect(response.statusCode).toBe(200);
                expect(result.bicicletas.length).toBe(0);
                done();
            })
        });
    });

    describe("POST BICICLETAS /create", ()=> {
        it("status 200", (done)=> {
            var headers = {'content-type': 'application/json'};
            var aBici = '{"Code": 30", "color": "rojo", "modelo": "urbana", "last": -34}'
            request.post({
                headers: headers,
                url:    base_url + '/create',
                body: aBici
            }, function(error, response, body){
                expect(response.statusCode).toBe(200);
                var bici = JSON.parse(body).bicicleta;
                console.log(bici);
                expect(bici.color).toBe("rojo");
                expect(bici.ubicacion[0]).toBe(-34);
                expect(bici.ubicacion[1]).toBe(-54);
                done();
            });
        });
    });
    describe('DELETE BICICLETAS /delete', () => {
    });
})

describe('Bicicleta API', () => {
    describe('GET BICICLETAS /', () => {
        it('status 200', () =>{
            expect(Bicicleta.allBicis.length).toBe(0);

            var a = new Bicicleta (1, 'rojo', 'urbana', [3.4079914,-76.5246838]);
            Bicicleta.add(a);

            request.get('http://localhost:3000/api/bicicletas', function(error,response,body){
                expect(response.statusCode).toBe(200);
            });
        });
    });

});